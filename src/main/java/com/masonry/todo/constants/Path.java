package com.masonry.todo.constants;

public class Path {

	
	private static final String API_VERSION = "/v1";
	private static final String PATH_VARIABLE_ID = "/{id}";
	
	public static final String TODOS = API_VERSION+"/todos";
	public static final String FIND_ALL_TODO = API_VERSION+"/tasks";
	public static final String TODO = TODOS+PATH_VARIABLE_ID;
	public static final String SEARCH_TASK = API_VERSION+"/task_search";
	
	
	

}
