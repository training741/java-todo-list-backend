package com.masonry.todo.constants;

public final class Messages {

	public static final String EMPTY_BODY_REQUEST = "Invalid request object: Request body cannot be empty or null";
	public static final String INVALID_TASK_DESCRIPTION = "Invalid request object: Task cannot have an invalid description";
	public static final String INVALID_RESOURCE_LOCATION = "Invalid resource location: please check your URL and your resource id";

}
