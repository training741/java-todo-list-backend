package com.masonry.todo.controllers;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import com.masonry.todo.data.repository.todos.Task;

@Component
public class TodoModelAssembler implements RepresentationModelAssembler<Task, EntityModel<Task>> {
	//private String searchTerm="";
	@Override
	public EntityModel<Task> toModel(Task todo) {
		return EntityModel.of(todo, 
				WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TodoController.class).findOne(todo.getId())).withSelfRel(),
				WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TodoController.class).findAll()).withRel("todos"));
	}
	/*public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}*/

	
	

}
