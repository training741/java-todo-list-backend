package com.masonry.todo.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.masonry.todo.constants.Messages;
import com.masonry.todo.constants.Path;
import com.masonry.todo.data.repository.todos.Task;
import com.masonry.todo.data.repository.todos.TodoRepository;
import com.masonry.todo.exception.BadRequestException;
import com.masonry.todo.exception.ResourceNotFoundException;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class TodoController {
	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private TodoRepository todoRepository;
	
	private final TodoModelAssembler todoAssembler;
	
	public TodoController(TodoModelAssembler todoAssembler) {
		this.todoAssembler = todoAssembler;
	}
	
	
	@GetMapping(path=Path.TODOS, produces = MediaType.APPLICATION_JSON_VALUE)
	public CollectionModel<EntityModel<Task>>findAll(){
		List<Task>tasksFound = todoRepository.findAll();
		logger.info("===========Getting all tasks");
		 
		tasksFound.forEach((task)-> logger.info("*Task {} : {}",task.getId(), task.toString()));
		List<EntityModel<Task>> todos = todoRepository.findAll()
				.stream().map(todoAssembler::toModel)
				.collect(Collectors.toList());
		return CollectionModel.of(todos, linkTo(methodOn(TodoController.class)
				.findAll()).withSelfRel());
	}
	
	
	@GetMapping(Path.FIND_ALL_TODO)
	public List<Task>getAll(){
		return todoRepository.findAll();
	}
	
	
	@GetMapping(Path.TODO)
	public EntityModel<Task> findOne(@PathVariable("id") Long id){
		
		Task todo = todoRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException(id));
		logger.info("===========Got one task "+ todo.toString() );
		return todoAssembler.toModel(todo);
	}
	
	
	@PostMapping(path=Path.TODOS
			, consumes = MediaType.APPLICATION_JSON_VALUE
			, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EntityModel<Task>> createTask(@RequestBody Task task){
		if (task == null)
			throw new BadRequestException(Messages.EMPTY_BODY_REQUEST);
		if(task.hasInvalidDescription())
			throw new BadRequestException(Messages.INVALID_TASK_DESCRIPTION);
		logger.info("===========Creating new task "+ task.toString() );
		
		EntityModel<Task> createdTask = todoAssembler.toModel(todoRepository.add(task));
		//logger.info("===========New task created"+ justCreated.toString() );
		return ResponseEntity
				.created(createdTask.getRequiredLink(IanaLinkRelations.SELF).toUri())
				.body(createdTask);
	}
	
	
	@PutMapping(path=Path.TODO
			, consumes = MediaType.APPLICATION_JSON_VALUE
			, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EntityModel<Task>> updateTask(@PathVariable("id")Long id, @RequestBody Task update){
		if(!id.equals(update.getId()))
			throw new BadRequestException(Messages.INVALID_RESOURCE_LOCATION);
		if(update.hasInvalidDescription())
			throw new BadRequestException(Messages.INVALID_TASK_DESCRIPTION);
		logger.info("===========task update"+ update.toString() );
		todoRepository.findById(update.getId()).orElseThrow(()-> new ResourceNotFoundException(update.getId()));
		Task tUpdated = todoRepository.update(update);
		EntityModel<Task> updated = todoAssembler.toModel(tUpdated);
		return ResponseEntity
				.created(updated.getRequiredLink(IanaLinkRelations.SELF).toUri())
				.body(updated);
	}

}
