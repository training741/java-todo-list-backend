package com.masonry.todo.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.masonry.todo.constants.Path;
import com.masonry.todo.data.repository.todos.Task;
import com.masonry.todo.data.repository.todos.TodoRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TaskSearchController {
	
	private final Logger logger = LogManager.getLogger(getClass());
	
	private TodoRepository taskRepository;
	
	private final TodoModelAssembler todoAssembler;
	
	public TaskSearchController(TodoRepository todoRepository, TodoModelAssembler todoAssembler) {
		super();
		this.taskRepository = todoRepository;
		this.todoAssembler = todoAssembler;
	}



	@GetMapping(path=Path.SEARCH_TASK, produces = MediaType.APPLICATION_JSON_VALUE)
	/*
	 * if no default value specified the request parameter is mandatory
	 */
	public CollectionModel<EntityModel<Task>>findAll(@RequestParam(name="term",defaultValue = "")String term){
		List<Task>tasksFound = new ArrayList<>();
		if(term==null || term.trim().isBlank()) 
			tasksFound	= taskRepository.findAll();
		else tasksFound = taskRepository.searchByTerm(term);
		logger.info("===========Getting tasks matching term {}",term);
		
		tasksFound.forEach((task)-> logger.info("** Task {} : {}",task.getId(), task.toString()));
		List<EntityModel<Task>> todos = tasksFound
				.stream().map(todoAssembler::toModel)
				.collect(Collectors.toList());
		return CollectionModel.of(todos, linkTo(methodOn(TodoController.class).findAll()).withSelfRel());
	}

}
