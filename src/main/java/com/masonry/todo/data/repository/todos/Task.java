package com.masonry.todo.data.repository.todos;

import java.time.LocalDateTime;

public class Task {
	
	private Long id=(long)0;
	private String title;
	private boolean completed=false;
	private LocalDateTime createdAt = LocalDateTime.now();
	
	public Task() {
		
	}
	
	

	public Task(Long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}



	public Task(String title) {
		super();
		this.title = title;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public boolean isCompleted() {
		return completed;
	}



	public void setCompleted(boolean completed) {
		this.completed = completed;
	}



	public LocalDateTime getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	public boolean hasInvalidDescription() {
		
		return title==null || title.isEmpty();
	}
	
	public boolean titleContainsTerm(String term) {
		return title.contains(term);
	}

	@Override
	public String toString() {
		return "Todo [id=" + id + ", title=" + title + ", completed=" + completed + ", createdAt=" + createdAt + "]";
	}
	
	
	
	
	
	

}
