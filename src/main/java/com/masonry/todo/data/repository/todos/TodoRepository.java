package com.masonry.todo.data.repository.todos;

import java.util.List;
import java.util.Optional;

public interface TodoRepository {

	List<Task> findAll();

	Task add(Task todo);

	Optional<Task> findById(Long id);

	Task update(Task update);

	List<Task> searchByTerm(String emptyTerm);
	

}
