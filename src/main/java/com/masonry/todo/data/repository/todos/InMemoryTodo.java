package com.masonry.todo.data.repository.todos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class InMemoryTodo implements TodoRepository {
	private Long lastGeneratedId = (long)0;
	private Map<Long, Task> todoDatabase = new HashMap<>();
	
	@Override
	public List<Task> findAll() {
		if(todoDatabase.isEmpty())
			return new ArrayList<>();
		return todoDatabase.values().stream().collect(Collectors.toList());
	}

	@Override
	public Task add(Task todo) {
		todo.setId(generateId());
		todoDatabase.put(todo.getId(), todo);
		return getOne(todo.getId());
	}

	public Task getOne(Long id) {
		return todoDatabase.getOrDefault(id, new Task());
	}

	@Override
	public Optional<Task> findById(Long id) {
		if(todoDatabase.containsKey(id))
			return Optional.of(todoDatabase.get(id));
		return Optional.empty();
	}


	private Long generateId() {
		lastGeneratedId = lastGeneratedId+1;
		return lastGeneratedId;
	}



	@Override
	public Task update(Task update) {
		todoDatabase.replace(update.getId(), update);
		return getOne(update.getId());
	}

	@Override
	public List<Task> searchByTerm(String term) {
		return todoDatabase.values()
				.stream()
				.filter(task -> task.titleContainsTerm(term))
				.collect(Collectors.toList());
	}


	

}
