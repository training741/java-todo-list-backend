package com.masonry.todo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTodoListBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaTodoListBackendApplication.class, args);
	}

}
