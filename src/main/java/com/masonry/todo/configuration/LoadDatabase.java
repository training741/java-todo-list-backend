package com.masonry.todo.configuration;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.masonry.todo.data.repository.todos.Task;
import com.masonry.todo.data.repository.todos.TodoRepository;

@Configuration
public class LoadDatabase {
	private static final Logger logger = LogManager.getLogger(LoadDatabase.class);
	
	@Bean
	CommandLineRunner initiate(TodoRepository todoRepository) {
		return args -> {
			Task taskOne = new Task("Todo one");
			taskOne.setCreatedAt(LocalDateTime.of(2022, 11, 19, 15, 0));
			todoRepository.add(taskOne);
			todoRepository.add(new Task("Todo two"));
			todoRepository.add(new Task("Todo three"));
			todoRepository.add(new Task("Todo four"));
			todoRepository.add(new Task("Todo five"));
			
			todoRepository.findAll().forEach(todo -> 
			logger.info("to do \"{}\" saved",todo.getTitle()));
		};
		
	};

}
