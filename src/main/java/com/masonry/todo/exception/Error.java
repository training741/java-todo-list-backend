package com.masonry.todo.exception;

import java.time.LocalDateTime;

public class Error {
	private LocalDateTime timestamp= LocalDateTime.now();
	private String message;
	private int status;
	
	
	public Error(String message, int status) {
		super();
		this.message = message;
		this.status = status;
	}
	
	
	public Error() {
		super();
	}


	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
