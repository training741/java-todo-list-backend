package com.masonry.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.masonry.todo.constants.Path;
import com.masonry.todo.controllers.TaskSearchController;
import com.masonry.todo.controllers.TodoModelAssembler;
import com.masonry.todo.data.repository.todos.Task;
import com.masonry.todo.data.repository.todos.TodoRepository;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {TaskSearchController.class, TodoModelAssembler.class})
@WebMvcTest
@TestInstance(Lifecycle.PER_CLASS)
public class TaskSearchControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private TodoRepository taskRepository;
	
	@Autowired
	ObjectMapper objectMapper;
	
	private JacksonTester<Task> jsonWriter;
	
	@BeforeAll
	public void setUp() {
		jsonWriter.initFields(this, objectMapper);
	}
	
	public List<Task> returnFiveMockTodo() {
		List<Task> listOfTodo= new ArrayList<>();
		for(int i=0;i<5;i++) {
			long id = i+1;
			String description = "Todo"+id;
			listOfTodo.add(new Task(id, description));
		}
		return listOfTodo;
	}
	
	@Test
	public void testSearchWithEmptyTerm() throws Exception{
		String emptyTerm = "";
		 BDDMockito.given(taskRepository.searchByTerm(emptyTerm)).willReturn(returnFiveMockTodo());
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(Path.SEARCH_TASK).param("term", emptyTerm)
				.accept(APPLICATION_JSON_VALUE))
		.andExpect(status().isOk())
		.andReturn();
		
		assertNotNull(result.getResponse().getContentAsString());
	}
	


}
