package com.masonry.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.masonry.todo.constants.Messages;
import com.masonry.todo.constants.Path;
import com.masonry.todo.controllers.TodoController;
import com.masonry.todo.controllers.TodoModelAssembler;
import com.masonry.todo.data.repository.todos.Task;
import com.masonry.todo.data.repository.todos.TodoRepository;
import com.masonry.todo.exception.BadRequestException;
import com.masonry.todo.exception.ResourceNotFoundException;


/*@SpringBootTest(classes = {TodoController.class, TodoModelAssembler.class, ObjectMapper.class})
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@TestInstance(Lifecycle.PER_CLASS)*/

@AutoConfigureMockMvc
@ContextConfiguration(classes = {TodoController.class,TodoModelAssembler.class})
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@WebMvcTest
@TestInstance(Lifecycle.PER_CLASS)

public class TodoControllerTest {
	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mockMvc;

	@MockBean
	private TodoRepository todoRepository;
	
	@Autowired
	private ObjectMapper objectMapper;
	private JacksonTester<Task>taskJTester;

	@BeforeAll
	public void configure() {
		JacksonTester.initFields(this, objectMapper);
	}
	@BeforeEach
	public  void setup(RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
				.apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation))
				.build();
	}
	
	


	public List<Task> returnFiveMockTodo() {
		List<Task> listOfTodo= new ArrayList<>();
		for(int i=0;i<5;i++) {
			long id = i+1;
			String description = "Todo"+id;
			listOfTodo.add(new Task(id, description));
		}
		return listOfTodo;
	}

	@Test
	public void testFindAllRetruningEmptyList() throws Exception {
		BDDMockito.given(todoRepository.findAll()).willReturn(new ArrayList<>());
		MvcResult result = mockMvc.perform(get(Path.TODOS))
		.andExpect(status().isOk())
		.andReturn();
		
		String responseBody = result.getResponse().getContentAsString();
		assertNotNull(responseBody);   //  {"_links":{"self":{"href":"http://localhost:8080/v1/todos"}}}>
		//assertTrue(responseBody.equals("{\"_links\":{\"self\":{\"href\":\"http://localhost/v1/todos\"}}}"));
	}

	@Test
	public void testFindAll() throws Exception{
		 BDDMockito.given(todoRepository.findAll()).willReturn(returnFiveMockTodo());
		 MvcResult result = mockMvc.perform(get(Path.TODOS))
				 .andDo(MockMvcResultHandlers.print())
				 .andExpect(status().isOk())
				 .andDo(MockMvcRestDocumentation.document("get-tasks"
						 , PayloadDocumentation.responseFields(
								 subsectionWithPath("_embedded.taskList")
								 .description("Array of tasks <<resources_tasks, Task resources>>")
								 ,subsectionWithPath("_links.self").description("links")
								 )))
				 .andReturn();
		 
		 String body = result.getResponse().getContentAsString();
		 assertNotNull(body);
		 
	}
	
	@Test
	void testFindById() throws Exception {
		long id = 1;
		Task mockfound = new Task(id, "todo mock");
		BDDMockito.given(todoRepository.findById(id)).willReturn(Optional.of(mockfound));
		MvcResult result = mockMvc.perform(get(Path.TODO, id))
				.andExpect(status().isOk())
				.andReturn();
		String responseBody = result.getResponse().getContentAsString();
		assertNotNull(responseBody);
	}

	//TODO : a better test must be done
	@Test
	public void testFindByIdShouldThrowTodoNotFoundException() throws Exception{
		long idNotFound = 1;
		BDDMockito.given(todoRepository.findById(idNotFound)).willReturn(Optional.empty());
		NestedServletException exception = assertThrows(NestedServletException.class,()->mockMvc.perform(get(Path.TODO, idNotFound)));
		assertTrue(exception.getCause() instanceof ResourceNotFoundException);
	
	}
	// TESTING POST METHOD (CREATE TASK)
	
	//@Test
	public void testCreateTask() throws Exception {
		BDDMockito.given(todoRepository.add(new Task(" mock todo saved")));
		 mockMvc.perform(MockMvcRequestBuilders.post(Path.TODOS))
		.andExpect(status().isBadRequest());
	}
	
	//@Test
	public void testCreateTaskReturnBadRequestForNullRequestBody() throws Exception {
		//String bodyRequest = taskJTester.write("").getJson();
		NestedServletException exception = assertThrows(NestedServletException.class,()->
				mockMvc.perform(MockMvcRequestBuilders.post(Path.TODOS)
						.accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content("{}")));
		assertTrue(exception.getCause() instanceof BadRequestException);
		
	}
	
	@Test
	public void testCreateTaskReturnBadRequestWithEmptyDescription() throws Exception{
		String bodyRequest = taskJTester.write(new Task("")).getJson();
		NestedServletException exception = assertThrows(NestedServletException.class,()->
		mockMvc.perform(MockMvcRequestBuilders.post(Path.TODOS)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(bodyRequest)));
		assertTrue(exception.getCause() instanceof BadRequestException);
		BadRequestException cause = (BadRequestException) exception.getCause();
		assertEquals(Messages.INVALID_TASK_DESCRIPTION,cause.getMessage());
		
	}
	
	@Test
	public void testCreateTaskReturnBadRequestWithNullDescription() throws Exception{
		String bodyRequest = taskJTester.write(new Task(null)).getJson();
		NestedServletException exception = assertThrows(NestedServletException.class,()->
		mockMvc.perform(MockMvcRequestBuilders.post(Path.TODOS)
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(bodyRequest)));
		assertTrue(exception.getCause() instanceof BadRequestException);
		BadRequestException cause = (BadRequestException) exception.getCause();
		assertEquals(Messages.INVALID_TASK_DESCRIPTION,cause.getMessage());
		
	}
	
	@Test
	public void testCreatedTaskRetrunCreated() throws Exception{
		long mockId = 1;
		String mockDescription = "mock description";
		Task mockPosted = new Task(mockDescription);
		Task mockCreated = new Task(mockId, mockDescription);
		String jsonMockPosted = taskJTester.write(mockPosted).getJson();
		BDDMockito.given(todoRepository.add(ArgumentMatchers.any(Task.class))).willReturn(mockCreated);
		
		 mockMvc.perform(MockMvcRequestBuilders.post(Path.TODOS)
				.accept(APPLICATION_JSON_VALUE)
				.contentType(APPLICATION_JSON_VALUE)
				.content(jsonMockPosted))
		.andExpect(status().isCreated())
		.andDo(MockMvcRestDocumentation.document("create-task", PayloadDocumentation
				.requestFields(fieldWithPath("id").ignored()
						,fieldWithPath("title").description("task title"),
						fieldWithPath("completed").description("the task is finished or not")
						,fieldWithPath("createdAt").ignored()
						)));
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(Path.TODOS)
				.accept(APPLICATION_JSON_VALUE)
				.contentType(APPLICATION_JSON_VALUE)
				.content(jsonMockPosted))
		.andExpect(status().isCreated())
		.andDo(MockMvcRestDocumentation.document("create-task",
				PayloadDocumentation.responseFields(fieldWithPath("id").description("task ID")
						, fieldWithPath("title").description("task title")
						,fieldWithPath("completed").description("Tells if the task is completed or not")
						,fieldWithPath("createdAt").description("Date and time when the task has been created")
						, subsectionWithPath("_links.self").description("link to the curent resource")
						, PayloadDocumentation.subsectionWithPath("_links.todos").description("link to all tasks"))))
		.andReturn();
		
		assertNotNull(result.getResponse().getContentAsString());
		
				
	}
	
	@Test
	public void testUpdateTaskWhenReturningBadRequestBecauseOfEmptyTaskDescription() throws Exception{
		long mockId=7;
		Task mockTaskToUpdate = new Task(mockId,"");
		String jsonMockBodyRequest = taskJTester.write(mockTaskToUpdate).getJson();
		BDDMockito.given(todoRepository.update(mockTaskToUpdate)).willReturn(mockTaskToUpdate);
		NestedServletException exception = assertThrows(NestedServletException.class, 
				()-> mockMvc.perform(MockMvcRequestBuilders.put(Path.TODO,mockId)
				.accept(APPLICATION_JSON_VALUE)
				.contentType(APPLICATION_JSON_VALUE)
				.content(jsonMockBodyRequest)));
		assertTrue(exception.getCause() instanceof BadRequestException);
		BadRequestException cause = (BadRequestException)exception.getCause();
		assertEquals(Messages.INVALID_TASK_DESCRIPTION, cause.getMessage());
	}
	
	@Test
	public void testUpdateTaskWhenReturningBadRequestBecuaseOfNullTaskDescription() throws Exception{
		long mockId=7;
		Task mockTaskToUpdate = new Task(mockId,null);
		String jsonMockBodyRequest = taskJTester.write(mockTaskToUpdate).getJson();
		BDDMockito.given(todoRepository.update(mockTaskToUpdate)).willReturn(mockTaskToUpdate);
		NestedServletException exception = assertThrows(NestedServletException.class, 
				()-> mockMvc.perform(MockMvcRequestBuilders.put(Path.TODO,mockId)
				.accept(APPLICATION_JSON_VALUE)
				.contentType(APPLICATION_JSON_VALUE)
				.content(jsonMockBodyRequest)));
		assertTrue(exception.getCause() instanceof BadRequestException);
		BadRequestException cause = (BadRequestException)exception.getCause();
		assertEquals(Messages.INVALID_TASK_DESCRIPTION, cause.getMessage());
		
	}
	
	@Test
	public void testUpdateTaskWhenReturningNotFound() throws Exception{
		long mockId=451;
		Task mockTaskToUpdate = new Task(mockId,"mock desc");
		String jsonMockBodyRequest = taskJTester.write(mockTaskToUpdate).getJson();
		BDDMockito.given(todoRepository.findById(mockId)).willReturn(Optional.empty());
		NestedServletException exception = assertThrows(NestedServletException.class, 
				()-> mockMvc.perform(MockMvcRequestBuilders.put(Path.TODO,mockId)
				.accept(APPLICATION_JSON_VALUE)
				.contentType(APPLICATION_JSON_VALUE)
				.content(jsonMockBodyRequest)));
		assertTrue(exception.getCause() instanceof ResourceNotFoundException);
		ResourceNotFoundException cause = (ResourceNotFoundException)exception.getCause();
		assertEquals("Could not found task "+mockId, cause.getMessage());
		
	}
	
	@Test
	public void testUpdateTask() throws Exception{
		long mockId = 7;
		String mockDescription = "task one";
		Task mockTodo = new Task(mockId, mockDescription);
		BDDMockito.given(todoRepository.findById(mockId)).willReturn(Optional.of(mockTodo));
		BDDMockito.given(todoRepository.update(ArgumentMatchers.any(Task.class))).willReturn(mockTodo);
		String mockJsonRequestBody = taskJTester.write(mockTodo).getJson();
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put(Path.TODO,mockId)
				.accept(APPLICATION_JSON_VALUE)
				.contentType(APPLICATION_JSON_VALUE)
				.content(mockJsonRequestBody))
		.andExpect(status().isCreated())
		.andReturn();
		assertNotNull(result.getResponse().getContentAsString());
		String bodyResponse = result.getResponse().getContentAsString();
		
	}
	
	@Test
	public void testUpdateTaskWhenThrowingInvalidResourceLocation() throws Exception{
		long mockId = 10001;
		long pathVariableId = 1001;
		Task mockTaskToUpdate = new Task(mockId,"unmatching resource");
		String mockJsonBodyRequest = taskJTester.write(mockTaskToUpdate).getJson();
		NestedServletException exception = assertThrows(NestedServletException.class,
				()-> mockMvc.perform(MockMvcRequestBuilders.put(Path.TODO, pathVariableId)
						.accept(APPLICATION_JSON_VALUE)
						.contentType(APPLICATION_JSON_VALUE)
						.content(mockJsonBodyRequest)));
		
		assertTrue(exception.getCause() instanceof BadRequestException);
		BadRequestException cause = (BadRequestException) exception.getCause();
		assertEquals(Messages.INVALID_RESOURCE_LOCATION, cause.getMessage());
	}
	
	
}
