package com.masonry.data.repository.todos;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.masonry.todo.data.repository.todos.InMemoryTodo;
import com.masonry.todo.data.repository.todos.Task;

public class InMemoryTodoTest {
	private InMemoryTodo inMemoryTodo;
	
	private void addTodo(int number) {
		for (int i=0;i<number;i++) {
			int count = i+1;
			String description = "Todo " + count;
			inMemoryTodo.add(new Task(description));
		}
		
	}
	
	@BeforeEach
	public void setUp() {
		inMemoryTodo = new InMemoryTodo();
	}
	
	
	@Test
	public void testEmptyArray() {
		assertEquals(new ArrayList<Task>(), inMemoryTodo.findAll());
	}
	
	@Test
	public void testFindAll() {
		addTodo(5);
		assertEquals(5, inMemoryTodo.findAll().size());
	}
	
	@Test
	public void testFindById() {
		Task todo = inMemoryTodo.add(new Task("to do one"));
		Optional<Task> found = inMemoryTodo.findById(todo.getId());
		
		assertTrue(found.isPresent());
		assertTrue(found.get().equals(todo));
	}
	
	@Test
	public void testFindByIdShouldReturnEmpty() {
		Optional<Task> found = inMemoryTodo.findById((long)1);
		assertTrue(found.isEmpty());
	}

	@Test
	public void testAddNewTodo() {
		Task todo = inMemoryTodo.add(new Task("to do one"));
		assertEquals(todo.getId(),(long)1);
		assertEquals(todo.getTitle(), "to do one");
		assertNotNull(todo.getCreatedAt());
		assertFalse(todo.isCompleted());
	}
	
	@Test
	public void testAddNewTodoDone() {
		Task forgottenTaskAlreadyDone = new Task("Done");
		forgottenTaskAlreadyDone.setCompleted(true);
		Task saved = inMemoryTodo.add(forgottenTaskAlreadyDone);
		assertTrue(saved.isCompleted());
	}
	
	@Test
	public void testUpdateTask() {
		String descUpdate = "updated desc";
		String desc ="desc to update";
		Task existingTask = new Task(desc);
		existingTask = inMemoryTodo.add(existingTask);
		assertEquals(1, existingTask.getId());
		assertFalse(existingTask.isCompleted());
		assertEquals(existingTask.getTitle(), desc);
		
		existingTask.setTitle(descUpdate);
		existingTask.setCompleted(true);
		
		Task updatedTask = inMemoryTodo.update(existingTask);
		assertEquals(existingTask.getTitle(), updatedTask.getTitle());
		assertTrue(updatedTask.isCompleted());
		
	}

}
