FROM eclipse-temurin:11-jdk-alpine
RUN addgroup -S todo && adduser -S todo -G todo
USER todo
VOLUME /tmp
COPY build/libs/java-todo-list-backend-0.0.1-SNAPSHOT.jar todo-list-backend-0.0.1.jar
ENTRYPOINT ["sh","-c","java ${JAVA_OPTS} -jar /todo-list-backend-0.0.1.jar"]